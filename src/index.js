import React from "react";
import ReactDOM from "react-dom";

import Router from "./components/Router";

import "./css/style.scss";

ReactDOM.render(<Router />, document.querySelector("#main"));
