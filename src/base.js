import Rebase from "re-base";
import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyD_ldEY4p8kW0Ae0OVlqKiesK0fBrzEiC0",
  authDomain: "el-palo-v2-react.firebaseapp.com",
  databaseURL: "https://el-palo-v2-react.firebaseio.com"
});

const base = Rebase.createClass(firebaseApp.database());

export { firebaseApp };

export default base;
