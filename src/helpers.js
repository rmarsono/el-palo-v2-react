export function formatPrice(cents) {
  return (cents / 100).toLocaleString("en-US", {
    style: "currency",
    currency: "USD"
  });
}

export function formatCop(cop) {
  return '$' + cop.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function rando(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export function getDateWithoutTime() {
  return new Date().toDateString();
}

export function getLastItemInObject(obj) {
  return Object.keys(obj)[Object.keys(obj).length - 1];
}

export function getPrettyDate() {
  return new Date().toLocaleString("en-AU");
}

export function getModifiedPrice(modifier, price) {
  if (!modifier || modifier === "0") return price;
  else {
    return ((parseFloat(modifier) + 100) / 100) * parseFloat(price);
  }
}

export function getRandomId() {
  return `${Date.now()}-${Math.round(Math.random() * 1000000)}`;
}

export function orderClosedWithinTwelveHours(rawTime) {
  if (!rawTime) return false;
  const twelveHours = new Date().getTime() + 12 * 60 * 60 * 1000;
  if (twelveHours > rawTime) return true;
  else return false;
}

export function isEmptyObject(obj) {
  for (var prop in obj) {
    return false;
  }
  return true;
}

export function slugify(text) {
  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w-]+/g, "")
    .replace(/--+/g, "-")
    .replace(/^-+/, "")
    .replace(/-+$/, "");
}
