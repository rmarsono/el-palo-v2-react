import React, { Component } from "react";

import StateContext from "./StateContext";

class AddProductForm extends Component {
  static contextType = StateContext;

  handleSubmit = event => {
    event.preventDefault();
    const { name, price, desc, isAvailable, productType } = event.currentTarget;
    this.context.addProduct({
      name: name.value,
      price: price.value,
      desc: desc.value,
      isAvailable: isAvailable.value,
      productType: productType.value
    });
    event.currentTarget.reset();
  };

  render() {
    const { productTypes } = this.context.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <input required type="text" placeholder="Product name" name="name" />
        <input
          required
          type="number"
          min="0"
          placeholder="Price"
          name="price"
        />
        <textarea name="desc" placeholder="Product description" />
        <select name="isAvailable">
          <option value="true">available</option>
          <option value="false">sold out!!!</option>
        </select>
        <select name="productType">
          {Object.keys(productTypes).map(key => (
            <option key={key} value={key}>
              {productTypes[key].name}
            </option>
          ))}
        </select>
        <button>+ add product</button>
      </form>
    );
  }
}

export default AddProductForm;
