import React, { Component } from "react";
import StateContext from "./StateContext";

class EditTable extends Component {
  static contextType = StateContext;

  handleChange = event => {
    this.context.updateTables(this.props.index, event.currentTarget.value);
  };

  handleClick = event => {
    this.context.updateTables(this.props.index, null);
  };

  render() {
    const { index } = this.props;
    const table = this.context.state.tables[index];
    return (
      <li>
        <input
          key={index}
          type="text"
          name="tableName"
          value={table}
          onChange={this.handleChange}
        />
        <button onClick={this.handleClick}>delete table</button>
      </li>
    );
  }
}

export default EditTable;
