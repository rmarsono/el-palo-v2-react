import React, { Component } from 'react'

import StateContext from './StateContext'
import { isEmptyObject } from '../helpers'

class Prefs extends Component {
  static contextType = StateContext

  componentDidMount() {
    const { prefs } = this.context.state
    if (isEmptyObject(prefs)) {
      this.context.setShowPrices(true)
    }
  }

  handleChange = event => {
    this.context.setShowPrices(event.currentTarget.value)
  }

  render() {
    const { showPrices } = this.context.state.prefs
    const boolShowPrices = showPrices === 'true'
    return (
      <div className="prefs-wrap">
        <h2>Prefs</h2>
        <div className="show-prices-wrap">
          <h3>Show prices</h3>
          <div className="fancy-checkbox-radio-group">
            <input
              type="radio"
              name="show-prices-radio"
              id="show-prices-radio-1"
              checked={boolShowPrices}
              value="true"
              onChange={this.handleChange}
            />
            <label htmlFor="show-prices-radio-1">yes</label>
            <input
              type="radio"
              name="show-prices-radio"
              id="show-prices-radio-2"
              checked={!boolShowPrices}
              value="false"
              onChange={this.handleChange}
            />
            <label htmlFor="show-prices-radio-2">no</label>
          </div>
        </div>
      </div>
    )
  }
}

export default Prefs
