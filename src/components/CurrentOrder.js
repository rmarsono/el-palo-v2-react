import React, { Fragment } from "react";

import { isEmptyObject } from "../helpers";
import ProductType from "./ProductType";
import OrderTotal from "./OrderTotal";
import OrderName from "./OrderName";
import TableSelector from "./TableSelector";
import PriceSelector from "./PriceSelector";
import StateContext from "./StateContext";

const CurrentOrder = () => {
  return (
    <StateContext.Consumer>
      {context => {
        const {
          currentOrder,
          orders,
          orderModifier,
          productTypes
        } = context.state;
        const { resetOrder, newOrder } = context;
        const selectedTable = orderModifier.table;
        const isDataAvailable =
          orders &&
          currentOrder &&
          currentOrder.orderNumber &&
          orders[currentOrder.orderId]
            ? true
            : false;
        let isCartEmpty = true;
        if (isDataAvailable)
          isCartEmpty = isEmptyObject(orders[currentOrder.orderId].products);
        return (
          <Fragment>
            {!isCartEmpty && (
              <div className="button-group">
                <button className="cta" onClick={() => newOrder()}>
                  new order
                </button>
                <button className="cta secondary" onClick={() => resetOrder()}>
                  reset order
                </button>
              </div>
            )}
            <h2>Bienvenido a El Palo</h2>
            <h3>
              <span className="table">
                {selectedTable !== "" && `Table: ${selectedTable}`}
              </span>
              <span className="order">
                {isDataAvailable &&
                  `Order #${currentOrder.orderNumber} created on ${
                    orders[currentOrder.orderId].live
                  }`}
              </span>
              {!isCartEmpty && <OrderName />}
            </h3>
            <OrderTotal />
            <div className="current-order-controls-wrap">
              <PriceSelector />
              <TableSelector />
            </div>
            <Fragment>
              {Object.keys(productTypes).map(key => (
                <ProductType key={key} index={key} />
              ))}
            </Fragment>
          </Fragment>
        );
      }}
    </StateContext.Consumer>
  );
};

export default CurrentOrder;
