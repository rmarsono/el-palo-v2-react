import React, { Component } from 'react'

import {
  getRandomId,
  getDateWithoutTime,
  getLastItemInObject,
  getPrettyDate,
  isEmptyObject
} from '../helpers'
import StateContext from './StateContext'
import base from '../base'

class StateProvider extends Component {
  constructor() {
    super()
    this.state = {
      products: {},
      orders: {},
      currentOrder: {},
      orderNumbers: {},
      productTypes: {},
      tables: {},
      pricing: {},
      orderModifier: {
        pricing: '',
        table: ''
      },
      prefs: {},
      currentUser: {}
    }
    this.localStorageOrderId = 'el-palo-order-id'
    this.localStorageCurrentUser = 'el-palo-auth-user'
  }

  componentDidMount() {
    this.firebaseProducts = base.syncState('products', {
      context: this,
      state: 'products'
    })
    this.firebaseOrders = base.syncState('orders', {
      context: this,
      state: 'orders'
    })
    this.firebasePricing = base.syncState('pricing', {
      context: this,
      state: 'pricing'
    })
    this.firebaseProductTypes = base.syncState('productTypes', {
      context: this,
      state: 'productTypes'
    })
    this.firebaseTables = base.syncState('tables', {
      context: this,
      state: 'tables'
    })
    this.firebaseOrderNumbers = base.syncState('orderNumbers', {
      context: this,
      state: 'orderNumbers'
    })
    this.firebaseOrderModifier = base.syncState('orderModifier', {
      context: this,
      state: 'orderModifier'
    })
    this.firebasePrefs = base.syncState('prefs', {
      context: this,
      state: 'prefs'
    })
    this.restoreLocalStorage()
    if (localStorage.getItem(this.localStorageCurrentUser)) {
      this.setState({
        currentUser: JSON.parse(
          localStorage.getItem(this.localStorageCurrentUser)
        )
      })
    }
  }

  componentWillUnmount() {
    base.removeBinding(this.firebaseProducts)
    base.removeBinding(this.firebaseOrders)
    base.removeBinding(this.firebasePricing)
    base.removeBinding(this.firebaseProductTypes)
    base.removeBinding(this.firebaseTables)
    base.removeBinding(this.firebaseOrderNumbers)
    base.removeBinding(this.firebaseOrderModifier)
    base.removeBinding(this.firebasePrefs)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState !== this.state
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.currentOrder !== this.state.currentOrder) {
      this.updateLocalStorage()
      const { orders, currentOrder } = this.state
      if (!isEmptyObject(orders) && !isEmptyObject(currentOrder)) {
        const thisOrder = orders[currentOrder.orderId]
        const orderModifier = {
          pricing: thisOrder.pricing,
          table: thisOrder.table
        }
        this.setState({ orderModifier })
      }
    }
  }

  restoreLocalStorage = () => {
    const localStorageRef = localStorage.getItem(this.localStorageOrderId)
    if (localStorageRef) {
      this.setState({ currentOrder: JSON.parse(localStorageRef) })
    } else this.setState({ currentOrder: {} })
  }

  updateLocalStorage = () => {
    localStorage.setItem(
      this.localStorageOrderId,
      JSON.stringify(this.state.currentOrder)
    )
  }

  setShowPrices = showPrices => {
    this.setState({
      prefs: {
        showPrices
      }
    })
  }

  addProduct = product => {
    const products = { ...this.state.products }
    products[`product-${getRandomId()}`] = product
    this.setState({ products })
  }

  updateProduct = (name, value, index) => {
    const products = { ...this.state.products }
    products[index][name] = value
    this.setState({ products })
  }

  deleteProduct = index => {
    const products = { ...this.state.products }
    products[index] = null
    this.setState({ products })
  }

  generateOrderNumber = () => {
    const orderNumbers = { ...this.state.orderNumbers }
    const date = getDateWithoutTime()
    if (orderNumbers[date]) {
      const orderNumber = orderNumbers[date]
      return parseFloat(orderNumber[getLastItemInObject(orderNumber)] + 1)
    } else {
      return 1
    }
  }

  addToOrder = index => {
    const { orders, orderNumbers } = {
      ...this.state
    }
    let { currentOrder } = { ...this.state }
    const date = getDateWithoutTime()
    let orderId
    if (isEmptyObject(currentOrder)) {
      orderId = `order-${getRandomId()}`
      const orderNumber = this.generateOrderNumber()
      orderNumbers[date] = {
        [orderId]: orderNumber
      }
      orders[orderId] = {
        live: getPrettyDate(),
        closed: '',
        products: {}
      }
      currentOrder = {
        orderId,
        orderNumber
      }
      this.setState({ currentOrder })
    } else {
      orderId = this.state.currentOrder.orderId
    }
    if (orders[orderId]) {
      if (orders[orderId].products)
        orders[orderId].products[index] =
          orders[orderId].products[index] + 1 || 1
      else
        orders[orderId].products = {
          [index]: 1
        }
      const { orderModifier } = { ...this.state }
      const { pricing, table } = orderModifier
      if (pricing !== '') {
        orders[orderId].pricing = pricing
      }
      if (table !== '') {
        orders[orderId].table = table
      }
      this.setState({ orders, orderNumbers })
    } else {
      this.setState({ currentOrder: {} })
      this.addToOrder(index)
    }
  }

  updateOrderTotal = total => {
    const { orders, currentOrder } = { ...this.state }
    orders[currentOrder.orderId].total = total
    this.setState({ orders })
  }

  removeFromOrder = (orderId, productId) => {
    const orders = { ...this.state.orders }
    const thisOrder = orders[orderId]
    if (thisOrder.products[productId] - 1 > 0) {
      thisOrder.products[productId] = thisOrder.products[productId] - 1
    } else thisOrder.products[productId] = null
    this.setState({ orders })
  }

  closeOrder = orderId => {
    const { orders } = { ...this.state }
    let { currentOrder } = { ...this.state }
    if (!orderId) {
      const { orderId } = currentOrder
      currentOrder = {}
    }
    orders[orderId].closed = getPrettyDate()
    orders[orderId].rawClosed = new Date().getTime()
    this.setState({ orders, currentOrder })
  }

  reopenOrder = orderId => {
    const { orders } = { ...this.state }
    orders[orderId].closed = ''
    this.setState({ orders })
  }

  newOrder = () => {
    const orderModifier = {
      pricing: '',
      table: ''
    }
    const currentOrder = {}
    this.setState({ orderModifier, currentOrder })
  }

  resetOrder = () => {
    const orderModifier = {
      pricing: '',
      table: ''
    }
    const { orders } = { ...this.state }
    orders[this.state.currentOrder.orderId] = null
    const currentOrder = {}
    this.setState({
      orderModifier,
      orders,
      currentOrder
    })
  }

  addPricing = (name, value) => {
    const { pricing } = { ...this.state }
    pricing[`pricing-${getRandomId()}`] = {
      name,
      value
    }
    this.setState({ pricing })
  }

  updatePricing = (key, name, value) => {
    const pricing = { ...this.state.pricing }
    pricing[key] = {
      name,
      value
    }
    this.setState({ pricing })
  }

  addProductType = (name, order) => {
    const productTypes = { ...this.state.productTypes }
    productTypes[`productType-${getRandomId()}`] = {
      name,
      order
    }
    this.setState({ productTypes })
  }

  updateProductTypes = (index, name, order) => {
    const productTypes = { ...this.state.productTypes }
    productTypes[index] = {
      name,
      order
    }
    this.setState({ productTypes })
  }

  addTable = tableName => {
    const tables = { ...this.state.tables }
    tables[`table-${getRandomId()}`] = tableName
    this.setState({ tables })
  }

  updateTables = (index, tableName) => {
    const tables = { ...this.state.tables }
    tables[index] = tableName
    this.setState({ tables })
  }

  updateSelectedPricing = pricing => {
    const { orderModifier } = { ...this.state }
    orderModifier.pricing = pricing
    this.setState({ orderModifier })
    const { currentOrder, orders } = { ...this.state }
    if (!isEmptyObject(currentOrder)) {
      const orderId = currentOrder.orderId
      orders[orderId].pricing = this.state.orderModifier.pricing
      this.setState({ orders })
    }
  }

  updateSelectedTable = table => {
    const { orderModifier } = { ...this.state }
    orderModifier.table = table
    this.setState({ orderModifier })
    const { currentOrder, orders } = { ...this.state }
    if (!isEmptyObject(currentOrder)) {
      const orderId = currentOrder.orderId
      orders[orderId].table = this.state.orderModifier.table
      this.setState({ orders })
    }
  }

  updateOrderName = orderName => {
    const { orders, currentOrder } = { ...this.state }
    orders[currentOrder.orderId].orderName = orderName
    this.setState({ orders })
  }

  activateOrder = orderId => {
    let orderNumber
    const { orderNumbers } = this.state
    Object.keys(orderNumbers).map(key => {
      const orderDate = orderNumbers[key]
      Object.keys(orderDate).map(key => {
        if (key === orderId) {
          orderNumber = orderDate[key]
          return
        }
      })
    })
    const currentOrder = {
      orderId,
      orderNumber
    }
    this.setState({ currentOrder })
  }

  setCurrentUser = currentUser => {
    this.setState({ currentUser })
    localStorage.setItem(
      this.localStorageCurrentUser,
      JSON.stringify(this.state.currentUser)
    )
  }

  render() {
    return (
      <StateContext.Provider
        value={{
          state: this.state,
          addProduct: this.addProduct,
          updateProduct: this.updateProduct,
          deleteProduct: this.deleteProduct,
          addToOrder: this.addToOrder,
          removeFromOrder: this.removeFromOrder,
          closeOrder: this.closeOrder,
          reopenOrder: this.reopenOrder,
          newOrder: this.newOrder,
          addPricing: this.addPricing,
          updatePricing: this.updatePricing,
          addProductType: this.addProductType,
          updateProductTypes: this.updateProductTypes,
          addTable: this.addTable,
          updateTables: this.updateTables,
          updateSelectedPricing: this.updateSelectedPricing,
          updateSelectedTable: this.updateSelectedTable,
          setShowPrices: this.setShowPrices,
          updateOrderTotal: this.updateOrderTotal,
          resetOrder: this.resetOrder,
          updateOrderName: this.updateOrderName,
          activateOrder: this.activateOrder,
          setCurrentUser: this.setCurrentUser
        }}
      >
        {this.props.children}
      </StateContext.Provider>
    )
  }
}

export default StateProvider
