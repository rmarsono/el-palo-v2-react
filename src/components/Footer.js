import React from 'react'

import Nav from './Nav'
import StateContext from './StateContext'
import { isEmptyObject } from '../helpers'

const Footer = () => {
  return (
    <StateContext.Consumer>
      {context => {
        const { currentUser } = context.state
        return (
          <footer>
            <div className="phone">
              Domicilios: <a href="tel:310 735 9025">310 735 9025</a>
            </div>
            <div className="address">
              Bienvenido al Parque Biosaludable, Buga - Valle
            </div>
            {!isEmptyObject(currentUser) && <Nav />}
          </footer>
        )
      }}
    </StateContext.Consumer>
  )
}

export default Footer
