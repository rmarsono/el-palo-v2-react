import React, { Component } from 'react'

import EditProductType from './EditProductType'
import StateContext from './StateContext'

class ProductTypes extends Component {
  static contextType = StateContext

  isProductTypeValid = (productTypes, productTypeName, productTypeOrder) => {
    const isInvalid = Object.keys(productTypes).some(productTypeId => {
      if (
        productTypes[productTypeId].name === productTypeName ||
        productTypes[productTypeId].order === productTypeOrder
      ) {
        return true
      } else return false
    })
    return !isInvalid
  }

  handleSubmit = event => {
    event.preventDefault()
    const { productTypeName, productTypeOrder } = event.currentTarget
    if (
      this.isProductTypeValid(
        this.context.state.productTypes,
        productTypeName.value,
        productTypeOrder.value
      )
    ) {
      this.context.addProductType(productTypeName.value, productTypeOrder.value)
      event.currentTarget.reset()
    }
  }

  render() {
    const { productTypes } = this.context.state
    return (
      <div className="product-types-wrap">
        <h2>Product types</h2>
        <ul>
          {Object.keys(productTypes).map(key => {
            return <EditProductType key={key} index={key} />
          })}
        </ul>
        <form onSubmit={this.handleSubmit}>
          <input
            type="text"
            placeholder="New product type"
            name="productTypeName"
            required
          />
          <input
            type="number"
            placeholder="Order"
            name="productTypeOrder"
            required
          />
          <button>+ add product type</button>
        </form>
      </div>
    )
  }
}

export default ProductTypes
