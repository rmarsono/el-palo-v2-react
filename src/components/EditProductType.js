import React, { Component, Fragment } from "react";

import StateContext from "./StateContext";

class EditProductType extends Component {
  static contextType = StateContext;

  handleChange = event => {
    const { productTypeName, productTypeOrder } = event.currentTarget;
    this.context.updateProductTypes(
      this.props.index,
      productTypeName.value,
      productTypeOrder.value
    );
  };

  handleClick = event => {
    this.context.updateProductTypes(this.props.index, null, null);
  };

  render() {
    const productType = this.context.state.productTypes[this.props.index];
    return (
      <Fragment>
        <li>
          <form onChange={this.handleChange}>
            <input
              type="text"
              value={productType.name}
              name="productTypeName"
            />
            <input
              type="number"
              value={productType.order}
              name="productTypeOrder"
            />
          </form>
        </li>
        <button onClick={this.handleClick}>delete product type</button>
      </Fragment>
    );
  }
}

export default EditProductType;
