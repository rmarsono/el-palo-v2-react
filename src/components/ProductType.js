import React, { Fragment } from "react";

import StateContext from "./StateContext";
import Product from "./Product";

const ProductType = props => {
  const { index } = props;
  return (
    <StateContext.Consumer>
      {context => {
        const { productTypes, products } = context.state;
        const productsWithThisProductTypeIds = Object.keys(products).filter(
          key => {
            if (products[key].productType === index) return key;
          }
        );
        return (
          <Fragment>
            <div className="product-type-wrap">
              <h4>{productTypes[index].name}</h4>
              <div className="products-wrap">
                {productsWithThisProductTypeIds.map(key => {
                  return <Product key={key} index={key} />;
                })}
              </div>
            </div>
          </Fragment>
        );
      }}
    </StateContext.Consumer>
  );
};

export default ProductType;
