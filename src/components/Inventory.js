import React from "react";

import StateContext from "./StateContext";
import AddProductForm from "./AddProductForm";
import EditProductForm from "./EditProductForm";

const Inventory = () => {
  return (
    <div className="inventory-wrap">
      <h2>Inventory</h2>
      <StateContext.Consumer>
        {context =>
          Object.keys(context.state.products).map(key => (
            <EditProductForm key={key} index={key} />
          ))
        }
      </StateContext.Consumer>
      <AddProductForm />
    </div>
  );
};

export default Inventory;
