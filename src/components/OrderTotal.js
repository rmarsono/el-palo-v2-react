import React from 'react';

import StateContext from './StateContext';
import { getModifiedPrice, isEmptyObject, formatCop } from '../helpers';

const OrderTotal = () => (
  <StateContext.Consumer>
    {context => {
      const { products, orders, pricing } = context.state;
      const currentOrder = orders[context.state.currentOrder.orderId];
      const isDataAvailable =
        products && currentOrder && currentOrder.products ? true : false;
      let orderTotal = 0;
      if (isDataAvailable) {
        const orderedProducts = currentOrder.products;
        orderTotal = Object.keys(orderedProducts).reduce((prevTotal, key) => {
          const boolIsAvailable = products[key].isAvailable === 'true';
          if (boolIsAvailable)
            return prevTotal + products[key].price * orderedProducts[key];
          else return prevTotal;
        }, 0);
        if (
          !isEmptyObject(pricing) &&
          !isEmptyObject(currentOrder) &&
          pricing[currentOrder.pricing]
        ) {
          orderTotal = getModifiedPrice(
            pricing[currentOrder.pricing].value,
            orderTotal
          );
        }
        if (orders[currentOrder]) {
          if (orders[currentOrder].total !== orderTotal)
            context.updateOrderTotal(orderTotal);
        } else context.updateOrderTotal(orderTotal);
      }
      return (
        <div className="order-total">
          {orderTotal > 0 && formatCop(orderTotal)}
        </div>
      );
    }}
  </StateContext.Consumer>
);

export default OrderTotal;
