import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import StateContext from './StateContext'
import { firebaseApp } from '../base'
import { isEmptyObject } from '../helpers'

class Nav extends Component {
  static contextType = StateContext

  state = {
    isNavShown: false
  }

  handleClick = () => {
    const isNavShown = !this.state.isNavShown
    this.setState({ isNavShown })
  }

  handleSignOut = () => {
    firebaseApp
      .auth()
      .signOut()
      .then(() => this.context.setCurrentUser(firebaseApp.auth().currentUser))
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    let navClassName = 'nav'
    if (this.state.isNavShown) navClassName += ' is-shown'
    const { currentUser } = this.context.state
    return (
      <div className="nav-wrap">
        <button onClick={this.handleClick} className="mobile-nav">
          {!this.state.isNavShown ? <span>&#9776;</span> : <span>&times;</span>}
        </button>
        <ul className={navClassName}>
          <li>
            <NavLink to="/" activeClassName="active" exact>
              store
            </NavLink>
          </li>
          <li>
            <NavLink to="/orders" activeClassName="active">
              orders
            </NavLink>
          </li>
          <li>
            <NavLink to="/admin" activeClassName="active">
              admin
            </NavLink>
          </li>
          <li>
            {!isEmptyObject(currentUser) ? (
              <button className="cta" onClick={this.handleSignOut}>
                sign out
              </button>
            ) : (
              <NavLink to="/login" activeClassName="active">
                sign in
              </NavLink>
            )}
          </li>
        </ul>
      </div>
    )
  }
}

export default Nav
