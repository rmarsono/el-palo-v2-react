import React from 'react'

import StateContext from './StateContext'
import { orderClosedWithinTwelveHours, formatCop } from '../helpers'

const OrderCard = props => {
  return (
    <StateContext.Consumer>
      {context => {
        const { orders, products, orderNumbers, pricing } = context.state
        const { index } = props
        const order = orders[index]
        const orderedProducts = order.products
        let orderNumber = null
        Object.keys(orderNumbers).map(key => {
          const orderDate = orderNumbers[key]
          if (orderDate[index]) orderNumber = orderDate[index]
        })
        if (orderedProducts) {
          return (
            <div className="order-card-wrap">
              <h4>
                Order #{orderNumber}
                {order.table && order.closed !== '' && (
                  <span className="table"> - table {order.table}</span>
                )}
                {order.orderName && (
                  <span className="order-name">({order.orderName})</span>
                )}
              </h4>
              <span className="live-date">
                {order.closed !== '' && 'live on'} {order.live}
              </span>
              {order.closed !== '' && (
                <span className="closed-date">closed on {order.closed}</span>
              )}
              <ul className="products-wrap">
                {Object.keys(orderedProducts).map(key => {
                  return (
                    <li key={key}>
                      {orderedProducts[key]} x {products[key].name}
                    </li>
                  )
                })}
              </ul>
              <div className="pricing-wrap">
                {order.pricing && pricing && pricing[order.pricing] && (
                  <span className="pricing">
                    (
                    {order.pricing === '0'
                      ? 'regular'
                      : pricing[order.pricing].name}
                    )
                  </span>
                )}
                {order.total && (
                  <span className="order-total">{formatCop(order.total)}</span>
                )}
              </div>
              {order.closed === '' ? (
                <div className="cta-group">
                  <button
                    className="cta"
                    onClick={() => context.closeOrder(index)}
                  >
                    close order
                  </button>
                  <button
                    className="cta secondary"
                    onClick={() => context.activateOrder(index)}
                  >
                    activate order
                  </button>
                </div>
              ) : orderClosedWithinTwelveHours(order.rawClosed) ? (
                <button
                  className="cta"
                  onClick={() => context.reopenOrder(index)}
                >
                  reopen order
                </button>
              ) : null}
            </div>
          )
        } else return null
      }}
    </StateContext.Consumer>
  )
}

export default OrderCard
