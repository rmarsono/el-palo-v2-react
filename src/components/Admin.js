import React from 'react'

import Inventory from './Inventory'
import Pricing from './Pricing'
import ProductTypes from './ProductTypes'
import Tables from './Tables'
import Prefs from './Prefs'
import StateContext from './StateContext'
import { isEmptyObject } from '../helpers'

const Admin = () => {
  return (
    <StateContext.Consumer>
      {context => {
        const { currentUser } = context.state
        return !isEmptyObject(currentUser) ? (
          <div className="admin-wrap">
            <Prefs />
            <Inventory />
            <Pricing />
            <ProductTypes />
            <Tables />
          </div>
        ) : (
          <div className="message error">
            You need to be logged in to view Admin
          </div>
        )
      }}
    </StateContext.Consumer>
  )
}

export default Admin
