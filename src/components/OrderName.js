import React, { Component } from "react";

import StateContext from "./StateContext";

class OrderName extends Component {
  static contextType = StateContext;

  handleChange = event => {
    this.context.updateOrderName(event.currentTarget.value);
  };

  render() {
    const { orders, currentOrder } = this.context.state;
    const id = "order-name";
    return (
      <div className="adaptive-placeholder-input order-name">
        <input
          type="text"
          value={orders[currentOrder.orderId].orderName}
          name="orderName"
          onChange={this.handleChange}
          id={id}
          placeholder='Order name'
        />
      </div>
    );
  }
}

export default OrderName;
