import React, { Fragment } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Store from './Store'
import NotFound from './NotFound'
import Header from './Header'
import Footer from './Footer'
import Admin from './Admin'
import StateProvider from './StateProvider'
import Orders from './Orders'
import Login from './Login'

const Router = () => {
  return (
    <StateProvider>
      <BrowserRouter>
        <Fragment>
          <Header />
          <div className="content-wrap">
            <Switch>
              <Route exact path="/" component={Store} />
              <Route exact path="/orders" component={Orders} />
              <Route exact path="/admin" component={Admin} />
              <Route exact path="/login" component={Login} />
              <Route component={NotFound} />
            </Switch>
          </div>
          <Footer />
        </Fragment>
      </BrowserRouter>
    </StateProvider>
  )
}

export default Router
