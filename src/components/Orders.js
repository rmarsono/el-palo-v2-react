import React from 'react'

import LiveOrders from './LiveOrders'
import ClosedOrders from './ClosedOrders'
import StateContext from './StateContext'
import { isEmptyObject } from '../helpers'

const Orders = () => {
  return (
    <StateContext.Consumer>
      {context => {
        const { currentUser } = context.state
        return !isEmptyObject(currentUser) ? (
          <div className="orders-wrap">
            <LiveOrders />
            <ClosedOrders />
          </div>
        ) : (
          <div className="message error">You need to be logged in to see Orders</div>
        )
      }}
    </StateContext.Consumer>
  )
}

export default Orders
