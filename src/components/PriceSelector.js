import React, { Component, Fragment } from "react";

import StateContext from "./StateContext";

class PriceSelector extends Component {
  static contextType = StateContext;

  handleChange = event => {
    this.context.updateSelectedPricing(event.currentTarget.value);
  };

  render() {
    const { pricing, orderModifier } = this.context.state;
    const defaultId = "price-selector-default";
    const selectedPricing = orderModifier.pricing;
    return (
      <div className="price-selector-wrap fancy-checkbox-radio-group">
        <input
          name="pricing-selector-radio"
          type="radio"
          value="0"
          checked={selectedPricing === "0"}
          onChange={this.handleChange}
          id={defaultId}
        />
        <label htmlFor={defaultId}>Regular</label>
        {Object.keys(pricing).map(key => {
          return (
            <Fragment key={key}>
              <input
                name="pricing-selector-radio"
                type="radio"
                value={key}
                checked={selectedPricing === key}
                onChange={this.handleChange}
                id={key}
              />
              <label htmlFor={key}>{pricing[key].name}</label>
            </Fragment>
          );
        })}
      </div>
    );
  }
}

export default PriceSelector;
