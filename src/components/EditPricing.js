import React, { Component } from "react";

import StateContext from "./StateContext";

class EditPricing extends Component {
  static contextType = StateContext;

  handleChange = event => {
    const { name, value } = event.currentTarget;
    this.context.updatePricing(this.props.index, name.value, value.value);
  };

  render() {
    const { pricing } = this.context.state;
    const thisPricing = pricing[this.props.index];
    return (
      <form
        className="edit-pricing-wrap"
        onChange={this.handleChange}
        onSubmit={() => this.context.updatePricing(this.props.index, null, null)}
      >
        <input type="text" name="name" value={thisPricing.name} />
        <label>
          <input type="text" name="value" value={thisPricing.value} /> %
        </label>
        <button>delete pricing</button>
      </form>
    );
  }
}

export default EditPricing;
