import React, { Component } from 'react'

import EditTable from './EditTable'
import StateContext from './StateContext'

class Tables extends Component {
  static contextType = StateContext

  handleSubmit = event => {
    event.preventDefault()
    const { tableName } = event.currentTarget
    this.context.addTable(tableName.value)
    event.currentTarget.reset()
  }

  render() {
    const { tables } = this.context.state
    return (
      <div className="tables-wrap">
        <h2>Tables</h2>
        <ul>
          {Object.keys(tables).map(key => {
            return <EditTable key={key} index={key} />
          })}
        </ul>
        <form onSubmit={this.handleSubmit}>
          <input type="text" name="tableName" placeholder="Table name" />
          <button>+ add table</button>
        </form>
      </div>
    )
  }
}

export default Tables
