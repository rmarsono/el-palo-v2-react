import React, { Component, Fragment } from "react";

import StateContext from "./StateContext";

class TableSelector extends Component {
  static contextType = StateContext;

  handleChange = event => {
    this.context.updateSelectedTable(event.currentTarget.value);
  };

  render() {
    const { tables, orderModifier } = this.context.state;
    const selectedTable = orderModifier.table;
    return (
      <div className="table-selector-wrap fancy-checkbox-radio-group">
        {Object.keys(tables).map(key => {
          return (
            <Fragment key={key}>
              <input
                value={tables[key]}
                checked={selectedTable === tables[key]}
                type="radio"
                name="table-selector-radio"
                onChange={this.handleChange}
                id={key}
              />
              <label htmlFor={key}>{tables[key]}</label>
            </Fragment>
          );
        })}
      </div>
    );
  }
}

export default TableSelector;
