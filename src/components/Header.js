import React from 'react'

import Logo from './Logo'
import Nav from './Nav'
import StateContext from './StateContext'
import { isEmptyObject } from '../helpers'

const Header = () => {
  return (
    <StateContext.Consumer>
      {context => {
        const { currentUser } = context.state
        return (
          <header>
            <Logo />
            {!isEmptyObject(currentUser) && <Nav />}
          </header>
        )
      }}
    </StateContext.Consumer>
  )
}

export default Header
