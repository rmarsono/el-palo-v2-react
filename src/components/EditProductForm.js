import React, { Component } from "react";

import StateContext from "./StateContext";

class EditProductForm extends Component {
  static contextType = StateContext;

  handleChange = event => {
    const { name, value } = event.currentTarget;
    this.context.updateProduct(name, value, this.props.index);
  };

  handleDelete = () => {
    this.context.deleteProduct(this.props.index);
  };

  render() {
    const { products, productTypes } = this.context.state;
    const { name, price, desc, isAvailable, productType } = products[
      this.props.index
    ];
    return (
      <div className="edit-product-form">
        <input
          type="text"
          placeholder="Product name"
          name="name"
          value={name}
          onChange={this.handleChange}
        />
        <input
          type="number"
          min="0"
          placeholder="Price"
          name="price"
          value={price}
          onChange={this.handleChange}
        />
        <textarea
          name="desc"
          placeholder="Product description"
          value={desc}
          onChange={this.handleChange}
        />
        <select
          onChange={this.handleChange}
          name="isAvailable"
          value={isAvailable}
        >
          <option value="true">available</option>
          <option value="false">sold out!!!</option>
        </select>
        <select
          onChange={this.handleChange}
          name="productType"
          value={productType}
        >
          {Object.keys(productTypes).map(key => (
            <option key={key} value={key}>
              {productTypes[key].name}
            </option>
          ))}
        </select>
        <button onClick={this.handleDelete}>delete product</button>
      </div>
    );
  }
}

export default EditProductForm;
