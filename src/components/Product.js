import React from 'react';

import StateContext from './StateContext';
import { getModifiedPrice, formatCop } from '../helpers';

const Product = props => {
  const getButtonCopy = price => {
    return ` ${price}`;
  };

  return (
    <StateContext.Consumer>
      {context => {
        const { index } = props;
        const { addToOrder, removeFromOrder } = context;
        const {
          products,
          currentOrder,
          orders,
          pricing,
          orderModifier
        } = context.state;
        const product = products[index];
        const { img, name, desc, isAvailable } = product;
        let { price } = product;
        const boolIsAvailable = isAvailable === 'true';
        const { orderId } = currentOrder;
        const count =
          orders[orderId] && boolIsAvailable && orders[orderId].products
            ? orders[orderId].products[index]
            : null;
        let wrapperClassName = 'product-wrap';
        if (!boolIsAvailable) wrapperClassName += ' sold-out';
        if (
          orders[orderId] &&
          orders[orderId].pricing &&
          orders[orderId].pricing !== '0'
        ) {
          if (
            orders[orderId] &&
            orders[orderId].pricing &&
            pricing[orders[orderId].pricing]
          ) {
            price = getModifiedPrice(
              pricing[orders[orderId].pricing].value,
              price
            );
          } else price = null;
        }
        return (
          <div className={wrapperClassName}>
            {img && img !== '' ? <img src={img} alt={name} /> : null}
            <div className="title">
              <h5>
                {name} {count && <span className="count">x {count}</span>}
              </h5>
              {count && price && (
                <span className="price">{formatCop(count * price)}</span>
              )}
            </div>
            <div className='cta-group'>
              <button
                className="cta"
                disabled={
                  !boolIsAvailable ||
                  (orderModifier.pricing === '' && orderModifier.table === '')
                }
                onClick={() => addToOrder(index)}
              >
                {boolIsAvailable
                  ? 'add to order' + (price && getButtonCopy(formatCop(price)))
                  : 'sold out'}
              </button>
              {count && (
                <button
                  className="cta secondary"
                  onClick={() => removeFromOrder(orderId, index)}
                >
                  remove from order
                </button>
              )}
            </div>
            {desc !== '' ? <p>{desc}</p> : null}
          </div>
        );
      }}
    </StateContext.Consumer>
  );
};

export default Product;
