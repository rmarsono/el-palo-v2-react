import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'

import StateContext from './StateContext'
import { firebaseApp } from '../base'
import { isEmptyObject } from '../helpers'

export default class Login extends Component {
  static contextType = StateContext

  state = {
    username: '',
    password: '',
    error: {}
  }

  handleSubmit = event => {
    event.preventDefault()
    const { username, password } = this.state
    firebaseApp
      .auth()
      .signInWithEmailAndPassword(username, password)
      .then(() => {
        this.context.setCurrentUser(firebaseApp.auth().currentUser)
      })
      .catch(error => {
        this.setState({ error })
      })
  }

  handleChange = event => {
    const { name, value } = event.currentTarget
    this.setState({ [name]: value })
  }

  render() {
    const { username, password, error } = this.state
    const { currentUser } = this.context.state
    const errorCode = error.code
    let errorMessage
    if (!isEmptyObject(error)) {
      if (errorCode.indexOf('invalid-email') >= 0)
        errorMessage = 'the email is invalid'
      else if (errorCode.indexOf('user-not-found') >= 0)
        errorMessage = 'the email is not found'
      else if (errorCode.indexOf('wrong-password') >= 0)
        errorMessage = 'the password is wrong'
    }
    return (
      <div className="login-wrap">
        {!isEmptyObject(error) && (
          <div className="message error">{errorMessage}</div>
        )}
        {!isEmptyObject(currentUser) ? (
          <Fragment>
            <div className="message success">
              User {currentUser.email} successfully logged in
            </div>
            <div className="cta-group">
              <Link className="cta" to="/">
                Go to Store
              </Link>
              <Link className="cta secondary" to="/orders">
                Go to Orders
              </Link>
            </div>
          </Fragment>
        ) : (
          <form
            className="adaptive-placeholder-input"
            onSubmit={this.handleSubmit}
          >
            <input
              type="text"
              name="username"
              value={username}
              placeholder="User name"
              onChange={this.handleChange}
            />
            <input
              type="password"
              name="password"
              value={password}
              placeholder="Password"
              onChange={this.handleChange}
            />
            <button className="cta">Log in</button>
          </form>
        )}
      </div>
    )
  }
}
