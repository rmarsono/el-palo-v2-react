import React, { Component } from 'react'

import OrderCard from './OrderCard'
import StateContext from './StateContext'

class LiveOrders extends Component {
  static contextType = StateContext

  render() {
    const { orders, tables } = this.context.state
    return (
      <div className="live-orders-wrap">
        <h2>Live orders</h2>
        <ul className="tables-list">
          {Object.keys(tables).map(key => {
            const thisTable = tables[key]
            const ordersIdsForThisTable = Object.keys(orders).filter(key => {
              if (orders[key].table && orders[key].table === thisTable)
                return key
            })
            const liveOrdersIdsForThisTable = ordersIdsForThisTable.filter(
              key => {
                if (orders[key].closed === '') return key
              }
            )
            return (
              <li key={key}>
                <h3>Table: {thisTable}</h3>
                {liveOrdersIdsForThisTable.length > 0 ? (
                  liveOrdersIdsForThisTable.map(key => {
                    return <OrderCard key={key} index={key} />
                  })
                ) : (
                  <span className="empty">vacia</span>
                )}
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}

export default LiveOrders
