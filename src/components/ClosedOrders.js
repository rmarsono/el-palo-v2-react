import React, { Component } from 'react'

import StateContext from './StateContext'
import OrderCard from './OrderCard'

class ClosedOrders extends Component {
  static contextType = StateContext

  state = {
    isOpen: false
  }

  handleClick = event => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  render() {
    const { orders } = this.context.state
    const closedOrderIds = Object.keys(orders).filter(key => {
      if (orders[key].closed !== '') return key
    })
    let className = 'order-cards'
    if (this.state.isOpen) className += ' visible'
    return (
      <div className="closed-orders-wrap">
        <h2 className={!this.state.isOpen && 'closed'} onClick={this.handleClick}>
          {!this.state.isOpen && 'view '}closed orders
        </h2>
        <div className={className}>
          {closedOrderIds.map(key => (
            <OrderCard key={key} index={key} />
          ))}
        </div>
      </div>
    )
  }
}

export default ClosedOrders
