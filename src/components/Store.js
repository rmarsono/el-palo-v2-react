import React from "react";

import CurrentOrder from "./CurrentOrder";

const Store = () => {
  return (
    <div className="store-wrap">
      <CurrentOrder />
    </div>
  );
};

export default Store;
