import React, { Component } from "react";

import EditPricing from "./EditPricing";
import StateContext from "./StateContext";

class Pricing extends Component {
  static contextType = StateContext;

  handleSubmit = event => {
    event.preventDefault();
    const { name, value } = event.currentTarget;
    this.context.addPricing(name.value, value.value);
    event.currentTarget.reset();
  };

  render() {
    const { pricing } = this.context.state;
    return (
      <div className="pricing-wrap">
        <h2>Pricing</h2>
        {Object.keys(pricing).map(key => {
          return <EditPricing key={key} index={key} />;
        })}
        <form className="new-pricing" onSubmit={this.handleSubmit}>
          <input name="name" placeholder="Enter new price name" type="text" />
          <label>
            <input
              name="value"
              placeholder="Enter new markup/discount amount"
              type="text"
            />{" "}
            %
          </label>
          <button>+ add pricing class</button>
        </form>
      </div>
    );
  }
}

export default Pricing;
